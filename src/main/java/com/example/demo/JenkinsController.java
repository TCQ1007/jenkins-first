package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author will.tuo
 * @date 2022/2/9 10:38
 */
@RestController
public class JenkinsController {

    @GetMapping("/")
    public String getJekins() {
        return "恭喜你成功了!";
    }

    @GetMapping("/getBoyFriend")
    public String getBoyFriend() {
        return "恭喜你获得一个男朋友";
    }

    @GetMapping("/getGirlFriend")
    public String getGirlFriend() {
        return "恭喜你获得一个女朋友";
    }

}
